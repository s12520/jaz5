package domain.services;

import java.util.ArrayList;
import java.util.List;

import domain.Comment;
import domain.Film;

public class FilmService {

	private static List<Film> db = new ArrayList<Film>();
	private static int currentId=1;
	private static int currentCommentId=1;
	
	public List<Film> getAll( ){
		return db;
	}
	
	public Film get(int id){
		for(Film f : db){
			if(f.getId()==id)
				return f;
		}
		return null;
	}
	
	public void add(Film f){
		f.setId(++currentId);
		db.add(f);
	}
	
	public void addComment( int filmId, Comment comment ) {
        Film film = get( filmId );
        comment.setId( currentId );
        setCurrentCommentId(getCurrentCommentId() + 1);
        film.getComments().add(comment);
       
    }
	
	public void update(Film film){
		for(Film f : db){
			if(f.getId()==film.getId()){
				f.setTitle(film.getTitle());
				f.setLength(film.getLength());
			}
		}
	}
	
	public void delete(Film f){
		db.remove(f);
	}

	public static int getCurrentCommentId() {
		return currentCommentId;
	}

	public static void setCurrentCommentId(int currentCommentId) {
		FilmService.currentCommentId = currentCommentId;
	}
}
